import os
import ../src/utils
import tempfile

block:

  let dir = tempfile.mkdtemp()
  let subDir = joinPath(dir, "Covid1")
  createDir(sub_dir)
  let filename = joinPath(subDir, "data_2020-01-01.json")
  writeFile(filename, "null")

  doAssert findDirs(dir) == @[subDir]
  doAssert parsePatientDir(subDir, nccid_data) == filename

# Most recent JSON selected
block:

  let dir = tempfile.mkdtemp()
  let subDir = joinPath(dir, "Covid1")
  createDir(sub_dir)
  let jsons = @[
    joinPath(subDir, "data_2020-01-01.json"),
    joinPath(subDir, "data_2020-02-01.json")
  ]
  for j in jsons:
    writeFile(j, "null")

  doAssert parsePatientDir(subDir, nccid_data) == jsons[1]
