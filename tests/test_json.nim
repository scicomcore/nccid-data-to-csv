import json
import ../src/utils


# Int
block:
  let j = %* 1
  let flattened = flatten_json(j)
  let expected = %* {"": 1}
  doAssert flattened == expected

# String
block:
  let j = %* "hello"
  let flattened = flatten_json(j)
  let expected = %* {"": "hello"}
  doAssert flattened == expected

# Array
block:
  let j = %* [1, "hello", {"a": 2}]
  let flattened = flatten_json(j)
  let expected = %* {
    "0": 1,
    "1": "hello",
    "2.a": 2
  }
  doAssert flattened == expected

# Simple object
block:

  let j = %* {"a": 1, "b": "hello"}

  let flattened = flatten_json(j)

  let expected = %* {
    "a": 1,
    "b": "hello",
  }
  doAssert flattened == expected


# Object: child array
block:

  let j = %* {"a": [1, 2]}

  let flattened = flatten_json(j)

  let expected = %* {
    "a.0": 1,
    "a.1": 2,
  }

  doAssert flattened == expected

# Object: array of arrays
block:

  let j = %* {"a": [[1], [2]]}

  let flattened = flatten_json(j)

  let expected = %* {
    "a.0.0": 1,
    "a.1.0": 2,
  }

  doAssert flattened == expected


# Object: Array of objects
block:

  let j = %* {"a": [{"b": 1}, {"c": 2}]}

  let flattened = flatten_json(j)

  let expected = %* {
    "a.0.b": 1,
    "a.1.c": 2,
  }

  doAssert flattened == expected

# Object: Nested object
block:

  let j = %* {
      "a": 1,
      "b": {
        "c": true
    }
  }

  let flattened = flatten_json(j)

  let expected = %* {
    "a": 1,
    "b.c": true,
  }

  doAssert flattened == expected

# Object: Nested array
block:

  let j = %* {
      "a": 1,
      "b": {
        "c": [2, 3]
    }
  }

  let flattened = flatten_json(j)

  let expected = %* {
    "a": 1,
    "b.c.0": 2,
    "b.c.1": 3,
  }

  doAssert flattened == expected

block:
  let j = """{"a": 1,"b": 2}"""

  let j2 = """{"a": 3, "c": 4}"""
  var header: seq[string] = @[]
  var rows: seq[seq[JsonNode]] = @[]

  let d = newJNull()
  update(j, header, rows, d)
  update(j2, header, rows, d)

  let expected_header = @["a", "b", "c"]
  doAssert(header == expected_header)

  let expected_rows = @[@[newJInt(1), newJInt(2)],
                        @[newJInt(3), newJNull(), newJInt(4)]]
  echo rows
  doAssert(rows == expected_rows)
