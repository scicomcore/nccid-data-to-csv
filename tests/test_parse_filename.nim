import options
import times
from ../src/utils import parse_file_info, NCCID_JSON_KIND

block:

  const filename = "data_2020-01-01.json"
  let res = parse_file_info(filename)
  let dt = initDateTime(01, mJan, 2020, 00, 00, 00, utc())

  let (kind, date) = res.get
  doAssert kind == nccid_data
  doAssert date == dt

block:

  const filename = "status_2020-01-01.json"
  let res = parse_file_info(filename)
  let dt = initDateTime(01, mJan, 2020, 00, 00, 00, utc())

  let (kind, date) = res.get
  doAssert kind == nccid_status
  doAssert date == dt

block:

  const filename = "nope_2020-01-01.json"
  let res = parse_file_info(filename)
  assert not res.isSome
