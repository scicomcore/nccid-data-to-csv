import json
import tempfile
import ../src/utils

block:

  let (_, name) = tempfile.mkstemp()

  let rows = @[@[newJInt(1), newJInt(2)],
    @[newJInt(3), newJNull(), newJInt(4)]]
  let header = @["a", "b", "c"]
  write_rows(name, rows, header)

  const expected = "a,b,c\n1,2,null\n3,null,4\n"
  let res = readFile(name)
  doAssert res == expected

block:

  var s = @["test"]
  let res = pad_seq(s, "this", 2)
  let expected = @["test", "this"]
  doAssert res == expected
