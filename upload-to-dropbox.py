#!/usr/bin/env python
# -*- coding: utf-8 -*-

import dropbox
import os
import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--input',
                        type=str,
                        help='Path to the application',
                        required=True)

    parser.add_argument('--output',
                        type=str,
                        help='File name of the zip to be uploaded to dropbox',
                        required=True)

    parser.add_argument('--token',
                        type=str,
                        help='Access token',
                        required=True)

    args = parser.parse_args()

    downloader = dropbox.Dropbox(args.token)

    with open(args.input, 'rb') as f:

        downloader.files_upload(f.read(),
                                args.output,
                                dropbox.files.WriteMode('overwrite'))
