**unreleased**
**Version 0.4.0**
- Do not generate CSV if no JSONs processed
- Replace initial datetime with option
**Version 0.3.0**
- Fix recursing on arrays of objects: This bug would have previously raised an exception
- Additional tests for different JSON objects
- Parse patient folders upfront, rather than iterate files
- Add progress bar for UI feedback
- Expand tilde from input paths
