# What?

`nccid_csv` is a program, written in [Nim](https://nim-lang.org/), to aggregate JSON metadata in the [National COVID-19 Chest Imaging Database](https://nhsx.github.io/covid-chest-imaging-database/) (NCCID) and convert the results to CSV files.

Note: If you're using Python to work with the NCCID clinical data, I recommend first looking at the [nccid_cleaning](https://github.com/nhsx/nccid-cleaning) Github repository.

For detailed information about the structure of NCCID, refer to the [docs](https://nhsx.github.io/covid-chest-imaging-database/data_access.html). This program assumes that the clinical data are stored in the following format:

```
<root>/training/data/CovidX/status_DATE.json
<root>/training/data/CovidX/data_DATE.json
```

where `X` is an integer (forming the patient ID when prefixed by `Covid`), and `DATE` is in the `YYYY-MM-DD` format. An example set of data for two patients would be


```
<root>/training/data/Covid12/status_2020-04-20.json
<root>/training/data/Covid12/data_2020-04-21.json
<root>/training/data/Covid12/data_2020-04-22.json

<root>/training/data/Covid102/status_2020-04-19.json
<root>/training/data/Covid102/data_2020-04-25.json
<root>/training/data/Covid102/data_2020-04-26.json
```

For which this program would then

1. aggregate the two files
    ```
    <root>/training/data/Covid12/data_2020-04-22.json
    <root>/training/data/Covid102/data_2020-04-26.json
    ```
    into a CSV file called `nccid_data.csv`

2. aggregate the two files
    ```
    <root>/training/data/Covid12/status_2020-04-20.json
    <root>/training/data/Covid102/status_2020-04-20.json
    ```
    into a CSV file called `nccid_status.csv`

Notice that only the most recent data are considered for the two kinds of clinical data (`data` and `status`).

Finally, note that schema do not need to be consistent: this program will generate a suitable header based on all keys, filling missing fields with `null`.

# Usage

```
nccid_csv --src=<root> --dest=<some-directory>
```

If you wanted to process only the `training` directory, the call would be

```
nccid_csv --src=<root>/training --dest=<some-directory>
```

# Install

Executables are available to download via Dropbox:

1. [Macos](https://www.dropbox.com/s/nquudbvbahzocs3/nccid_csv?dl=1)
2. [Linux](https://www.dropbox.com/s/3vvmfjdsrpgp468/nccid_csv?dl=1)
3. [Windows](https://www.dropbox.com/s/6wg2ut0gmhm2mf6/nccid_csv.exe?dl=1)

## Building

[Install Nim](https://nim-lang.org/install.html), clone the repository and, for a release build, run

```
nimble build -d:release --opt:size
```

To [cross-compile for Windows](https://nim-lang.github.io/Nim/nimc.html#cross-compilation-for-windows) from Linux or macOS, use the MinGW-w64 toolchain:

```
nimble build -d:mingw --app:console -d:release --opt:size
```
(Use `--cpu:i386` or `--cpu:amd64` to switch the CPU architecture)

## Questions/Issues

If you have any problems, identify a bug, or would like to discuss an issue, please [create an issue](https://bitbucket.org/scicomcore/nccid-data-to-csv/issues?status=new&status=open).

## Acknowledgments

- Implementation support using approach in [rust-flatten-json](https://github.com/durch/rust-flatten-json/blob/master/src/lib.rs)
