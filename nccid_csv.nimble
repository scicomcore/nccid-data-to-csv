# Package

version = "0.4.0"
author = "Dominic Ward"
description = "Aggregate NCCID JSON data and convert to CSV"
license = "MIT"
srcDir = "src"
bin = @["nccid_csv"]

# Dependencies
requires "nim >= 1.4.0"
requires "regex >= 0.16.2"
requires "cligen >= 1.2.2"
requires "tempfile >= 0.1.7"
requires "progress >= 1.1.1"

task tests, "Run all tests":
  exec "testament cat ."

task buildrelease, "Build for platform":
  exec "nimble build -d:release --opt:size"

task buildwindows, "Cross-build for windows":
  exec "nimble build -d:mingw -d:release --opt:size"
