#!/usr/bin/env sh
TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
python3 ./upload-to-dropbox.py --input $1 --output "/$2-$TAG/$1" --token $DROPBOX_TOKEN
python3 ./upload-to-dropbox.py --input $1 --output "/$2-latest/$1" --token $DROPBOX_TOKEN
