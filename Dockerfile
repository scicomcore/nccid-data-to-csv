FROM 'nimlang/nim:latest-ubuntu-regular'
RUN apt-get update; apt-get install -y python3 python3-pip mingw-w64;
RUN pip3 install --no-cache-dir dropbox;
RUN ln -s /nim/bin/testament /usr/bin/testament;
