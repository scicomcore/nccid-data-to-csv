import cligen
import os
import streams
import json
import strutils, strformat
import regex
import times
import options
import tables
import sequtils
import times
import progress

type
  NCCID_JSON_KIND* = enum
    nccid_data, nccid_status


proc handle_object(j: JsonNode, table: JsonNode, parentKey: string = "")

proc handle_array(j: JsonNode, table: JsonNode,
    parentKey: string = "") =
  var i = 0
  var parent = parentKey
  if not parentKey.isEmptyOrWhitespace():
    parent.add(".")
  for val in j.items():
    let key = fmt"{parent}{i}"
    if val.kind == JArray:
      handle_array(val, table, key)
    elif val.kind == JObject:
      handle_object(val, table, key)
    else:
      table.add(key, val)
    i += 1

proc handle_object(j: JsonNode, table: JsonNode,
    parentKey: string = "") =
  var parent = parentKey
  if not parentKey.isEmptyOrWhitespace():
    parent.add(".")

  for key, val in j.pairs():
    let key = fmt"{parent}{key}"
    if val.kind == JObject:
      handle_object(val, table, key)
    elif val.kind == JArray:
      handle_array(val, table, key)
    else:
      table.add(key, val)

proc flatten_json*(j: JsonNode): JsonNode =

  let flattened = newJObject()
  if j.kind == JArray:
    handle_array(j, flattened)
  elif j.kind == JObject:
    handle_object(j, flattened)
  else:
    flattened.add("", j)
  return flattened

proc parse_file_info*(file: string): Option[(NCCID_JSON_KIND, DateTime)] =
  const regex = re".*(data|status)_(\d+-\d+-\d+).json"
  var m: RegexMatch
  if match(file, regex, m):
    try:
      let data_kind = m.group(0, file)[0]
      let date = m.group(1, file)[0]
      let dt = times.parse(date, "yyyy-MM-dd")
      if data_kind == "data":
        return some((nccid_data, dt))
      else:
        return some((nccid_status, dt))
    except:
      return none((NCCID_JSON_KIND, DateTime))

proc parsePatientDir*(dir: string, desired_data_kind: NCCID_JSON_KIND): string =

  var
    data_date: Option[DateTime]
    data_file: string

  for file in walkFiles(joinPath(dir, "*.json")):
    let res = parse_file_info(file)
    if not res.isSome():
      continue
    let (data_kind, date) = res.get()

    if data_kind != desired_data_kind:
      continue

    if data_date.isNone():
      data_date = some(date)
      data_file = file
    elif date > data_date.get():
      data_date = some(date)
      data_file = file

  return data_file


proc findDirs*(dir: string): seq[string] =
  for resource in walkDirRec(dir, yieldFilter = {pcDir}, checkDir = true):
    if extractFilename(resource).startsWith("Covid"):
      result.add(resource)
  if len(result) == 0:
    echo("Warning, no valid files found in ", dir)


proc update*(s: string | Stream,
             header: var seq[string],
             rows: var seq[seq[JsonNode]],
             default: JsonNode,
             ) =
  let j = json.parseJson(s)
  let data = flatten_json(j)
  for key in data.keys():
    if not (key in header):
      header.add(key)

  var result = newSeq[JsonNode](len(header))
  for i, key in header:
    if data.hasKey(key):
      result[i] = data[key]
    else:
      result[i] = default

  rows.add(result)


proc clean_line(line: seq[string], delimiter: string): string =
  for idx, el in line:
    if (
      el.contains(delimiter) and
        not (el.startsWith("\"") and el.endsWith("\""))
    ):
      result.add("\"" & el & "\"")
    else:
      result.add(el)
    if idx != line.high():
      result.add(delimiter)
  return result

proc pad_seq*[T](s: seq[T], with: T, length: int): seq[T] =
  if length > len(s):
    return sequtils.concat(s, sequtils.repeat(with, length - len(s)))
  return s

proc write_rows*(dest: string, rows: seq[seq[JsonNode]], header: seq[string]) =
  const delimiter = ","
  let default = newJNull()
  var strm = newFileStream(dest, fmWrite)
  defer: strm.close()
  strm.writeLine(clean_line(header, delimiter))
  for row in rows:
    let padded = pad_seq(row, default, len(header))
    strm.writeLine(padded.join(delimiter))

proc parse_and_write*(src: string, dest: string,
    desired_data_kind: NCCID_JSON_KIND) =

  let
    dirs = findDirs(src)
    t0 = getTime()
    default = newJNull()
  var
    header: seq[string] = @[]
    rows: seq[seq[JsonNode]] = @[]
    num_skipped = 0
    num_got = 0
    pb = newProgressBar(len(dirs), width = 80)
  echo fmt"Found {len(dirs)} directories...processing"
  for dir in dirs:
    let json_data_path = parsePatientDir(dir, desired_data_kind)
    if fileExists(json_data_path):
      try:
        update(readFile(json_data_path), header, rows, default)
        num_got.inc
      except JsonParsingError:
        num_skipped.inc
    pb.increment()
  pb.finish()

  let elapsed = getTime() - t0
  echo fmt"Processed {len(dirs)} directories in {elapsed}"

  if num_skipped > 0:
    echo fmt"Warning: Skippied {num_skipped} JSONs due to parsing errors"

  if num_got > 0:
    write_rows(dest, rows, header)
  else:
    echo "No JSONS found: CSV file will not be generated"
