import cligen
import os
import utils
import strformat


proc main(src: string, dest: string) =
  ## Aggregate and convert NCCID JSON data to CSV

  let src = expandTilde(src)
  let dest = expandTilde(dest)

  for path in @[src, dest]:
    if not dirExists(path):
      raise newException(IOError, fmt"{path} is not a directory")

  echo "Processing 'data' JSONs, please wait..."
  parse_and_write(src, dest / "nccid_data.csv", nccid_data)
  echo "Processing 'status' JSONs, please wait..."
  parse_and_write(src, dest / "nccid_status.csv", nccid_status)


when isMainModule:
  const nimbleData = staticRead("../nccid_csv.nimble")
  clCfg.version = nimbleData.fromNimble("version")

  dispatch(
    main,
    help = {
    "src": "Source directory containing NCCID",
    "dest": "Destination directory to write CSV files to"
    }
  )
